((root) => {
  'use strict';

  root.customMatchers = root.customMatchers || {};

  root.customMatchers.toHaveClass = () => {
    return {
      compare: (elem, className) => {
        let result = {};

        if (!angular.isElement(elem)) {
          throw new Error(`Expect HTMLNode, but got ${ typeof elem }`);
        } else if (typeof className !== 'string') {
          throw new Error(`Expect String, but got ${ typeof className }`);
        } else if (!angular.element(elem).hasClass(className)) {
          result.pass = false;
        } else {
          result.pass = true;
        }

        return result;
      }
    };
  };
})(this);
