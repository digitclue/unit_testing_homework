angular.module('truthNews').provider('errorNotifierSrv', function () {
  var providedShowTime;

  this.setErrorShowTime = function (time) {
    providedShowTime = time;
  };

  this.$get = ['$log', '$timeout', 'ERROR_NOTIFIER_DEFAULT_SHOW_TIME',
      function ($log, $timeout, ERROR_NOTIFIER_DEFAULT_SHOW_TIME) {
    var errors = [],
        showTime = providedShowTime || ERROR_NOTIFIER_DEFAULT_SHOW_TIME;

    function removeError (error) {
      var errorIndex = errors.indexOf(error);

      if (errorIndex > -1) {
        $timeout.cancel(error.removeTimeout);
        errors.splice(errorIndex, 1);
      } else {
        $log.error('errorNotifierSrv: error not found while trying to remove');
      }
    }

    function addError (error) {
      errors.push(error);

      error.removeTimeout = $timeout(function () {
        removeError(error);
      }, showTime);
    }

    function getErrors () {
      return errors;
    }

    return {
      addError: addError,
      getErrors: getErrors,
      removeError: removeError
    }
  }]
});
