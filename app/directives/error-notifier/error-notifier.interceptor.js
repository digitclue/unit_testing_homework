angular.module('truthNews').service('errorNotifierInterceptor',
  ['$q', 'errorNotifierSrv', function ($q, errorNotifierSrv) {
    return {
      responseError: function (error) {
        var errorMessage = [error.status || '', error.statusText || 'Data loading failed.'].join(' ');

        errorNotifierSrv.addError({message: errorMessage});

        return $q.reject(error);
      }
    }
  }]
);
