describe('loader', () => {
  'use strict';

  const HIDDEN_CLASS = 'hide';
  const PROGRESS_BAR_SELECTOR = '.progress-bar';

  beforeEach(module('truthNews'));
  beforeEach(() => {
    jasmine.addMatchers(customMatchers);
  });

  describe('service', () => {
    let loaderSrv;

    beforeEach(inject((_loaderSrv_) => {
      loaderSrv = _loaderSrv_;
    }));

    it('has disabled state from the start', () => {
      expect(loaderSrv.getLoadingState()).toBeFalsy();
    });

    it('can be switched to enabled state', () => {
      loaderSrv.show();
      expect(loaderSrv.getLoadingState()).toBeTruthy();
    });

    it('can be switched to disabled state', () => {
      loaderSrv.show();
      expect(loaderSrv.getLoadingState()).toBeTruthy();
      loaderSrv.hide();
      expect(loaderSrv.getLoadingState()).toBeFalsy();
    });
  });

  describe('directive', () => {
    let parentScope,
      $scope,
      element,
      mocks;

    beforeEach(() => {
      mocks = {
        loaderSrv: {
          getLoadingState: function () {
            return false;
          }
        }
      };

      module(($provide) => {
        $provide.value('loaderSrv', mocks.loaderSrv);
      });
    });

    beforeEach(inject(($rootScope, $compile) => {
      parentScope = $rootScope.$new();
      element = $compile('<div loader bar-color="{{ barColor }}"></div>')(parentScope);

      parentScope.$digest();
    }));

    it('has isolate scope', () => {
      $scope = element.isolateScope();

      expect($scope).toBeDefined();
    });

    it('is shown when enabled', () => {
      mocks.loaderSrv.getLoadingState = function () {
        return true;
      };

      parentScope.$digest();

      expect(element).not.toHaveClass(HIDDEN_CLASS);
    });

    it('is hidden when disabled', () => {
      mocks.loaderSrv.getLoadingState = function () {
        return false;
      };

      parentScope.$digest();

      expect(element).toHaveClass(HIDDEN_CLASS);
    });

    it('sets progress bar style depending on parameter barColor', () => {
      let progressBarElem = element[0].querySelector(PROGRESS_BAR_SELECTOR);

      parentScope.barColor = 'red';

      parentScope.$digest();

      expect(progressBarElem).toHaveClass('progress-bar-red');

      parentScope.barColor = 'green';

      parentScope.$digest();

      expect(progressBarElem).not.toHaveClass('progress-bar-red');
      expect(progressBarElem).toHaveClass('progress-bar-green');
    });
  });
});
